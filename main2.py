import requests
import re

def get_ips_by_asn(asn):
    url = f"https://bgp.he.net/AS{asn}#_prefixes"

    response = requests.get(url)

    if response.status_code == 200:
        html_content = response.text
        ips = re.findall(r'\b(?:\d{1,3}\.){3}\d{1,3}/\d{1,2}\b', html_content)
        return ips
    else:
        return None

# 4766
asn_number = input("Zadej číslo AS: ")
ips = get_ips_by_asn(asn_number)

if ips:
    unique_ips = set(ips)
    with open("vystup.txt", "w") as file:
        for ip in unique_ips:
            if ip != "0.0.0.0/0":
                file.write(f"{ip}\n")
        print("Hotovo")
else:
    print(f"Nepodařilo se získat IP adresy pro ASN {asn_number}.")
