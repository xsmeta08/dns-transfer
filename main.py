import re

def extract_and_format_subdomains(input_text):
    pattern = r'\b(\w+(?:\.\w+)*\.(?:cz|eu|com|sk))\b'
    matches = re.findall(pattern, input_text)

    formatted_subdomains = list(set(f'^{re.escape(subdomain)}' for subdomain in matches))

    return formatted_subdomains

def write_subdomains_to_file(formatted_subdomains, output_file):
    with open(output_file, 'w') as file:
        for formatted_subdomain in formatted_subdomains:
            file.write(formatted_subdomain + '\n')

input_file_path = 'input.txt'  # cesta ke vstupnímu souboru
with open(input_file_path, 'r') as input_file:
    input_text = input_file.read()

formatted_subdomains = extract_and_format_subdomains(input_text)

output_file_path = 'output.txt'  # název výstupního souboru
write_subdomains_to_file(formatted_subdomains, output_file_path)

