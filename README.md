**Pro DNS REGEX:**

1. Otevřít: https://trinket.io/features/python3
2. Vytvořit tam soubor main.py, input.txt (do main.py nahrát soubor main.py odsud a do input.txt vstup od zákazníka).
3. Do souboru output.txt (není nutné vytvářet) se nahraje formátovaný výstup, který už je možné nahrát přímo do Arboru.

**Pro ASN:**

- Otevřít: https://trinket.io/features/python3
- Do toho mainu co tam už je, tak vložit main2.py odsud (otevřít, zkopírovat a vložit), kliknout na start -> zadat pouze **ČÍSLO** AS.
- Do souboru output.txt (není nutné vytvářet) se nahraje formátovaný výstup, který už je možné nahrát přímo do Arboru.

